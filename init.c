/*
** init.c for init in /home/brosol_m/docs/systeme_unix_avance/strace
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun May 13 17:03:18 2012 matthias brosolo
** Last update Sun May 13 17:03:18 2012 matthias brosolo
*/

#include	<string.h>
#include	"strace.h"

void	init_opt(int ac, char **av, t_strace *s)
{
  if (!strcmp(av[1], "-p"))
    {
      if (ac == 3)
	s->pid = atoi(av[2]);
      else
	die("strace", "-p option needs pid\n");
    }
  else
    s->pid = 0;
}

void	init_strace(t_strace *st)
{
  st->reg_array[RDI] = &st->reg.rdi;
  st->reg_array[RSI] = &st->reg.rsi;
  st->reg_array[RDX] = &st->reg.rdx;
  st->reg_array[RCX] = &st->reg.rcx;
  st->reg_array[R8] = &st->reg.r8;
  st->reg_array[R9] = &st->reg.r9;
  st->print[VOID] = &print_void;
  st->print[SIGNED] = &print_signed;
  st->print[UNSIGNED] = &print_unsigned;
  st->print[POINTER] = &print_pointer;
  st->print[VARIABLE] = &print_variable;
}
