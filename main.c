/*
** main.c for main ex2 in /home/brosol_m/docs/systeme_unix_avance/strace/tp/ex2
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Mon Apr 23 14:33:06 2012 matthias brosolo
** Last update Mon Apr 23 14:33:06 2012 matthias brosolo
*/

#include	"strace.h"

void		catch_killed(int sig)
{
  (void) sig;
  if (ptrace(PTRACE_DETACH, get_traced_pid(0), NULL, NULL) == -1)
    die("ptrace(DETACH)", NULL);
  exit(0);
}

pid_t		get_traced_pid(pid_t tmp)
{
  static pid_t	pid = 0;

  if (tmp)
    pid = tmp;
  printf("PID => %d\n\n", pid);
  return (pid);
}

int		main(int ac, char **av)
{
  t_strace	s;

  if (ac < 2)
    return (usage(av[0]));
  init_opt(ac, av, &s);
  init_strace(&s);
  if (!s.pid)
    start_trace_bin(&s, av);
  else
    {
      get_traced_pid(s.pid);
      if (signal(SIGINT, &catch_killed) == SIG_ERR)
	die("signal()", NULL);
      if (signal(SIGQUIT, &catch_killed) == SIG_ERR)
	die("signal()", NULL);
      trace_pid(&s);
      if (((s.sys & 0xffff) == 0x80cd || (s.sys & 0xffff) == 0x050f
	   || (s.sys & 0xffff) == 0x340fcd)
	  && s.reg.rax <= __NR_recvmmsg)
	fprintf(stderr, ") = ?\n");
    }
  return (EXIT_SUCCESS);
}
