/*
** trace.c for trace in /home/brosol_m/docs/systeme_unix_avance/strace
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun May 13 21:04:07 2012 matthias brosolo
** Last update Sun May 13 21:04:07 2012 matthias brosolo
*/

#include	"strace.h"

extern	t_syscall	g_syscall_array[];

void		ex_bin(char **av)
{
  if (ptrace(PTRACE_TRACEME, 0, NULL, NULL) == -1)
    die("ptrace()", NULL);
  if (execvp(av[1], &av[1]) == -1)
    die("execvp()", NULL);
}

t_bool		trace_all(pid_t pid, t_strace *s)
{
  int		status;

  wait4(pid, &status, 0, NULL);
  if (WIFEXITED(status))
    return (FALSE);
  if (ptrace(PTRACE_GETREGS, pid, NULL, &s->reg) == -1)
    die("ptrace(GETREGS)", NULL);
  s->sys = ptrace(PTRACE_PEEKDATA, pid, s->reg.rip, NULL);
  if (((s->sys & 0xffff) == 0x80cd || (s->sys & 0xffff) == 0x050f
       || (s->sys & 0xffff) == 0x340f) && s->reg.rax <= __NR_recvmmsg)
    print_arguments(&g_syscall_array[s->reg.rax], s, pid);
  if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
    return (FALSE);
  return (TRUE);
}

void		trace_bin_step(pid_t pid, t_strace *s)
{
  if (!print_first(s, pid))
    return;
  while (TRUE)
    if (!trace_all(pid, s))
      return;
}

void		start_trace_bin(t_strace *s, char **av)
{
  pid_t		pid;

  if ((pid = fork()) == -1)
    die("fork()", NULL);
  else if (pid)
    {
      trace_bin_step(pid, s);
      if (((s->sys & 0xffff) == 0x80cd || (s->sys & 0xffff) == 0x050f
	   || (s->sys & 0xffff) == 0x340fcd)
	  && s->reg.rax <= __NR_recvmmsg)
	fprintf(stderr, ") = ?\n");
    }
  else
    ex_bin(av);
}

void		trace_pid(t_strace *s)
{
  if (ptrace(PTRACE_ATTACH, s->pid, NULL, NULL) == -1)
    die("ptrace(ATTACH)", NULL);
  while (TRUE)
    if (!trace_all(s->pid, s))
      return;
}
