##
## Makefile for make ex2 in /home/brosol_m/docs/systeme_unix_avance/strace/tp/ex2
## 
## Made by matthias brosolo
## Login   <brosol_m@epitech.net>
## 
## Started on  Mon Apr 23 14:36:14 2012 matthias brosolo
## Last update Mon Apr 23 18:22:58 2012 anna texier
##

NAME		=	strace

SRC		=	main.c \
			error.c \
			init.c \
			print.c \
			print_arguments.c \
			trace.c \
			syscall_array.c \

OBJ		=	$(SRC:.c=.o)

INCDIR		=	-I includes
CFLAGS		=	-ansi -pedantic -W -Wall -Wextra $(INCDIR)

CC		=	gcc
RM		=	@rm -vf

$(NAME)		:	$(OBJ)
			$(CC) -o $(NAME) $(OBJ)

all		:	$(NAME)

%.o		:	%.c
			$(CC) $(CFLAGS) -c $< -o $@

clean		:
			$(RM) $(OBJ)

fclean		:	clean
			$(RM) $(NAME)
			$(RM) *~

re		:	fclean all

.PHONY		:	all clean fclean re
