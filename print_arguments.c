/*
** print_arguments.c for print_arguments in /home/brosol_m/docs/systeme_unix_avance/strace
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun May 13 17:31:22 2012 matthias brosolo
** Last update Sun May 13 17:31:22 2012 matthias brosolo
*/

#include	"strace.h"

void	print_void(unsigned long arg)
{
  (void) arg;
}

void	print_signed(unsigned long arg)
{
  fprintf(stderr, "%ld", (long) arg);
}

void	print_unsigned(unsigned long arg)
{
  fprintf(stderr, "%lu", arg);
}

void	print_pointer(unsigned long arg)
{
  if (arg)
    fprintf(stderr, "%p", (void *) arg);
  else
    fprintf(stderr, "NULL");
}

void	print_variable(unsigned long arg)
{
  (void) arg;
}
