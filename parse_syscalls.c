/*
** parse_syscalls.c for strace in /home/texier_a//projets/strace
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Mon Apr 23 18:19:52 2012 anna texier
** Last update Mon Apr 23 18:31:53 2012 anna texier
*/

#include	<sys/syscall.h>
#include	<sys/ptrace.h>
#include	<sys/wait.h>
#include	<stdio.h>
#include	"strace.h"

extern	t_sc	syscall_array[];

int		print_syscall(pid_t pid, t_regs *regs)
{
  int		status;

  if (regs->rax <= 299)
    printf("%s", syscall_array[regs->rax].name);
  else
    printf("%lu", regs->rax);
  if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
    die("Error with ptrace SINGLESTEP", NULL);
  wait4(pid, &status, 0, NULL);
  if (WIFEXITED(status))
    {
      printf("\nThe child tracing is now done.\n");
      return (1);
    }
  if (ptrace(PTRACE_GETREGS, pid, NULL, regs) == -1)
    die("Error with ptrace GETREGS", NULL);
  printf(" ret -> %lx\n", regs->rax);
  return (0);
}

int		parse_syscalls(const pid_t pid)
{
  long		inst;
  t_regs	regs;
  int		status;

  while (TRUE)
    {
      wait4(pid, &status, 0, NULL);
      if (WIFEXITED(status))
	{
      	  printf("\nThe child tracing is now done.\n");
	  return (0);
	}
      if (ptrace(PTRACE_GETREGS, pid, NULL, &regs) == -1)
	die("Error with ptrace GETREGS", NULL);
      inst = ptrace(PTRACE_PEEKDATA, pid, regs.rip, NULL);
      if ((inst & 0xffff) == 0x80cd || (inst & 0xffff) == 0x050f ||
	  (inst & 0xffff) == 0x340f)
	if (print_syscall(pid, &regs) == 1)
	  return (0);
      if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
	die("Error with ptrace SINGLESTEP", NULL);
    }
  return (0);
}
