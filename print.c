/*
** print.c for print in /home/brosol_m/docs/systeme_unix_avance/strace
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun May 13 16:55:32 2012 matthias brosolo
** Last update Sun May 13 16:55:32 2012 matthias brosolo
*/

#include	"strace.h"

extern	t_syscall	g_syscall_array[];

t_bool	print_first(t_strace *s, pid_t pid)
{
  int	status;

  wait4(pid, &status, 0, NULL);
  if (WIFEXITED(status))
    return (FALSE);
  if (ptrace(PTRACE_GETREGS, pid, NULL, &s->reg) == -1)
    die("ptrace(GETREGS)", NULL);
  fprintf(stderr, "%s(", g_syscall_array[s->reg.orig_rax].name);
  print_all(&g_syscall_array[s->reg.orig_rax], s);
  fprintf(stderr, ") = ");
  (s->print[(g_syscall_array[s->reg.orig_rax]).returned])(s->reg.rax);
  fprintf(stderr, "\n");
  if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
    die("ptrace(SINGLESTEP)", NULL);
  return (TRUE);
}

void	print_all(t_syscall *c, t_strace *s)
{
  unsigned int	i;

  i = 0;
  while (i < MAX_ARGUMENTS && i < c->nb_arguments)
    {
      if (i)
	fprintf(stderr, ", ");
      (s->print[c->arg_type[i]])(*(s->reg_array[i]));
      ++i;
    }
}

t_bool	print_arguments(t_syscall *c, t_strace *s, pid_t pid)
{
  int	status;

  fprintf(stderr, "%s(", c->name);
  print_all(c, s);
  fflush(stdout);
  if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
    die("ptrace(SINGLESTEP)", NULL);
  wait4(pid, &status, 0, NULL);
  if (WIFEXITED(status))
  	return (FALSE);
  if (ptrace(PTRACE_GETREGS, pid, NULL, &s->reg) == -1)
    return (FALSE);
  else
    {
      fprintf(stderr, ")\t= ");
      (s->print[c->returned])(s->reg.rax);
      fprintf(stderr, "\n");
    }
  return (TRUE);
}
