/*
** error.c for error in /home/brosol_m/docs/systeme_unix/nm-objdump
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Mar  7 09:51:20 2012 matthias brosolo
** Last update Mon Apr 23 18:32:08 2012 anna texier
*/

#include	<errno.h>
#include	<stdlib.h>
#include	<string.h>
#include	<unistd.h>
#include	<stdio.h>
#include	"error.h"

int	die(char *fct, char *error)
{
  int	err;

  err = errno;
  fprintf(stderr, "[%s]: %s\n", fct, (error ? (error) : strerror(err)));
  exit(EXIT_FAILURE);
}

int	ret_error(char *fct, char *error)
{
  int	err;

  err = errno;
  fprintf(stderr, "[%s]: %s\n", fct, (error ? (error) : strerror(err)));
  return (EXIT_FAILURE);
}

int	usage(char *name)
{
  fprintf(stderr, "[USAGE]: %s bin arg1 ... | -p pid\n", name);
  return (EXIT_FAILURE);
}
