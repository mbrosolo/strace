/*
** error.h for error.h in /home/brosol_m/docs/systeme_unix/my_ftp/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Mar 29 22:32:45 2012 matthias brosolo
** Last update Mon Apr 23 18:30:57 2012 anna texier
*/

#ifndef	__ERROR_H__
#define	__ERROR_H__

/*
** error.c
*/
int	die(char *, char *);
int	ret_error(char *, char *);
int	usage(char *);

#endif
