/*
** strace.h for strace in /home/brosol_m/docs/systeme_unix_avance/strace
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sat May  5 17:23:02 2012 matthias brosolo
** Last update Sat May  5 17:23:02 2012 matthias brosolo
*/

#ifndef	__STRACE_H__
# define	__STRACE_H__
# define	_BSD_SOURCE

# include	<stdio.h>
# include	<stdlib.h>
# include	<sys/ptrace.h>
# include	<sys/user.h>
# include	<sys/types.h>
# include	<sys/time.h>
# include	<sys/resource.h>
# include	<sys/wait.h>
# include	"error.h"
# include	"unistd_64.h"

/*
** DEFINES
*/
/*
** ENUMS
*/
typedef	enum	e_bool
  {
    FALSE = 0,
    TRUE
  }		t_bool;

typedef	enum	e_arg_type
  {
    VOID = 0,
    SIGNED,
    UNSIGNED,
    POINTER,
    VARIABLE,
    ARG_TYPE_NUM
  }		t_arg_type;

typedef	enum	e_reg_num
  {
    RDI = 0,
    RSI,
    RDX,
    RCX,
    R8,
    R9,
    REG_NUM
  }		t_reg_num;

typedef	enum	e_num
  {
    MAX_ARGUMENTS = 6
  }		t_num;

/*
** TYPEDEFS
*/
typedef	struct	user_regs_struct	t_reg;

/*
** STRUCTURES
*/
typedef	struct	s_strace
{
  long		sys;
  pid_t		pid;
  t_reg		reg;
  unsigned long	*reg_array[REG_NUM];
  void		(*print[ARG_TYPE_NUM])(unsigned long n);
}		t_strace;

typedef	struct	s_syscall
{
  unsigned int	id_syscall;
  char		*name;
  unsigned int	nb_arguments;
  t_arg_type	arg_type[MAX_ARGUMENTS];
  t_arg_type	returned;
}		t_syscall;

/*
** PROTOTYPES
*/
/*
** init.c
*/
void	init_opt(int ac, char **av, t_strace *s);
void	init_strace(t_strace *st);

/*
** main.c
*/
void		catch_killed(int sig);
pid_t		get_traced_pid(pid_t tmp);

/*
** print_arguments.c
*/
void	print_void(unsigned long arg);
void	print_signed(unsigned long arg);
void	print_unsigned(unsigned long arg);
void	print_pointer(unsigned long arg);
void	print_variable(unsigned long arg);

/*
** print.c
*/
void	print_all(t_syscall *c, t_strace *s);
t_bool	print_first(t_strace *s, pid_t pid);
t_bool	print_arguments(t_syscall *c, t_strace *s, pid_t pid);

/*
** trace .c
*/
void		ex_bin(char **av);
t_bool		trace_all(pid_t pid, t_strace *s);
void		trace_bin_step(pid_t pid, t_strace *s);
void		start_trace_bin(t_strace *s, char **av);
void		trace_pid(t_strace *s);

#endif /* __STRACE_H__ */
