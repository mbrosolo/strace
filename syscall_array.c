/*
** syscall_array.c for syscall_array in /home/brosol_m/docs/systeme_unix_avance/strace/tp/ex2
**
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
**
** Started on  Mon Apr 23 16:44:54 2012 matthias brosolo
** Last update Mon Apr 23 17:26:03 2012 anna texier
*/

#include	"strace.h"

t_syscall	g_syscall_array[] =
  {
    {__NR_read, "read", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_write, "write", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_open, "open", 3, {POINTER, SIGNED, UNSIGNED}, SIGNED},
    {__NR_close, "close", 1, {SIGNED}, SIGNED},
    {__NR_stat, "stat", 2, {POINTER, POINTER}, SIGNED},
    {__NR_fstat, "fstat", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_lstat, "lstat", 2, {POINTER, POINTER}, SIGNED},
    {__NR_poll, "poll", 3, {POINTER, UNSIGNED, SIGNED}, SIGNED},
    {__NR_lseek, "lseek", 3, {SIGNED, UNSIGNED, SIGNED}, UNSIGNED},
    {__NR_mmap, "mmap", 6, {POINTER, UNSIGNED, SIGNED, SIGNED, SIGNED,
			    UNSIGNED}, POINTER},
    {__NR_mprotect, "mprotect", 3, {POINTER, UNSIGNED, SIGNED}, SIGNED},
    {__NR_munmap, "munmap", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_brk, "brk", 1, {SIGNED}, POINTER},
    {__NR_rt_sigaction, "rt_sigaction", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_rt_sigprocmask, "rt_sigprocmask", 3, {SIGNED, POINTER, POINTER},
     SIGNED},
    {__NR_rt_sigreturn, "rt_sigreturn", 1, {UNSIGNED}, SIGNED},
    {__NR_ioctl, "ioctl", 3, {SIGNED, SIGNED, VARIABLE}, SIGNED},
    {__NR_pread64, "pread64", 4, {SIGNED, POINTER, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_pwrite64, "pwrite64", 4, {SIGNED, POINTER, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_readv, "readv", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_writev, "writev", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_access, "access", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_pipe, "pipe", 2, {POINTER, SIGNED}, SIGNED},
    {__NR_select, "select", 5, {SIGNED, POINTER, POINTER, POINTER, POINTER},
     SIGNED},
    {__NR_sched_yield, "sched_yield", 0, {0}, SIGNED},
    {__NR_mremap, "mremap", 5, {POINTER, UNSIGNED, UNSIGNED, SIGNED, VARIABLE},
     POINTER},
    {__NR_msync, "msync", 3, {POINTER, UNSIGNED, SIGNED}, SIGNED},
    {__NR_mincore, "mincore", 3, {POINTER, UNSIGNED, POINTER}, SIGNED},
    {__NR_madvise, "madvise", 3, {POINTER, UNSIGNED, SIGNED}, SIGNED},
    {__NR_shmget, "shmget", 3, {SIGNED, UNSIGNED, SIGNED}, SIGNED},
    {__NR_shmat, "shmat", 3, {SIGNED, POINTER, SIGNED}, POINTER},
    {__NR_shmctl, "shmctl", 3, {SIGNED, SIGNED, POINTER}, SIGNED},
    {__NR_dup, "dup", 1, {SIGNED}, SIGNED},
    {__NR_dup2, "dup2", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_pause, "pause", 0, {0}, SIGNED},
    {__NR_nanosleep, "nanosleep", 2, {POINTER, POINTER}, SIGNED},
    {__NR_getitimer, "getitimer", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_alarm, "alarm", 1, {UNSIGNED}, UNSIGNED},
    {__NR_setitimer, "setitimer", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_getpid, "getpid", 0, {0}, SIGNED},
    {__NR_sendfile, "sendfile64", 3, {SIGNED, SIGNED, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_socket, "socket", 3, {SIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_connect, "connect", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_accept, "accept", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_sendto, "sendto", 6, {SIGNED, POINTER, UNSIGNED, SIGNED, POINTER,
				UNSIGNED}, SIGNED},
    {__NR_recvfrom, "recvfrom", 6, {SIGNED, POINTER, UNSIGNED, SIGNED, POINTER,
				    POINTER}, SIGNED},
    {__NR_sendmsg, "sendmsg", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_recvmsg, "recvmsg", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_shutdown, "shutdown", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_bind, "bind", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_listen, "listen", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_getsockname, "getsockname", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_getpeername, "getpeername", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_socketpair, "socketpair", 4, {SIGNED, SIGNED, SIGNED, POINTER},
     SIGNED},
    {__NR_setsockopt, "setsockopt", 5, {SIGNED, SIGNED, SIGNED, POINTER,
					UNSIGNED}, SIGNED},
    {__NR_getsockopt, "getsockopt", 5, {SIGNED, SIGNED, SIGNED, POINTER,
					POINTER}, SIGNED},
    {__NR_clone, "clone", 5, {POINTER, SIGNED, SIGNED, POINTER, UNSIGNED},
     SIGNED},
    {__NR_fork, "fork", 0, {0}, SIGNED},
    {__NR_vfork, "vfork", 0, {0}, SIGNED},
    {__NR_execve, "execve", 3, {POINTER, POINTER, POINTER}, SIGNED},
    {__NR_exit, "exit", 1, {SIGNED}, VOID},
    {__NR_wait4, "wait4", 4, {UNSIGNED, POINTER, SIGNED, POINTER}, UNSIGNED},
    {__NR_kill, "kill", 2, {UNSIGNED, SIGNED}, SIGNED},
    {__NR_uname, "newuname", 1, {POINTER}, SIGNED},
    {__NR_semget, "semget", 3, {UNSIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_semop, "semop", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_semctl, "semctl", 4, {SIGNED, SIGNED, SIGNED, VARIABLE}, SIGNED},
    {__NR_shmdt, "shmdt", 1, {POINTER}, SIGNED},
    {__NR_msgget, "msgget", 2, {UNSIGNED, SIGNED}, SIGNED},
    {__NR_msgsnd, "msgsnd", 4, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_msgrcv, "msgrcv", 5, {SIGNED, POINTER, UNSIGNED, SIGNED, SIGNED},
     SIGNED},
    {__NR_msgctl, "msgctl", 3, {SIGNED, SIGNED, POINTER}, SIGNED},
    {__NR_fcntl, "fcntl", 3, {SIGNED, SIGNED, VARIABLE}, SIGNED},
    {__NR_flock, "flock", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_fsync, "fsync", 1, {SIGNED}, SIGNED},
    {__NR_fdatasync, "fdatasync", 1, {SIGNED}, SIGNED},
    {__NR_truncate, "truncate", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_ftruncate, "ftruncate", 2, {SIGNED, UNSIGNED}, SIGNED},
    {__NR_getdents, "getdents", 3, {UNSIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_getcwd, "getcwd", 2, {POINTER, UNSIGNED}, POINTER},
    {__NR_chdir, "chdir", 1, {POINTER}, SIGNED},
    {__NR_fchdir, "fchdir", 1, {SIGNED}, SIGNED},
    {__NR_rename, "rename", 2, {POINTER, POINTER}, SIGNED},
    {__NR_mkdir, "mkdir", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_rmdir, "rmdir", 1, {POINTER}, SIGNED},
    {__NR_creat, "creat", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_link, "link", 2, {POINTER, POINTER}, SIGNED},
    {__NR_unlink, "unlink", 1, {POINTER}, SIGNED},
    {__NR_symlink, "symlink", 2, {POINTER, POINTER}, SIGNED},
    {__NR_readlink, "readlink", 3, {POINTER, POINTER, UNSIGNED}, SIGNED},
    {__NR_chmod, "chmod", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_fchmod, "fchmod", 2, {SIGNED, UNSIGNED}, SIGNED},
    {__NR_chown, "chown", 3, {POINTER, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_fchown, "fchown", 3, {SIGNED, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_lchown, "lchown", 3, {POINTER, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_umask, "umask", 1, {UNSIGNED}, UNSIGNED},
    {__NR_gettimeofday, "gettimeofday", 2, {POINTER, POINTER}, SIGNED},
    {__NR_getrlimit, "getrlimit", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_getrusage, "getrusage", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_sysinfo, "sysinfo", 1, {POINTER}, SIGNED},
    {__NR_times, "times", 1, {POINTER}, UNSIGNED},
    {__NR_ptrace, "ptrace", 4, {SIGNED, UNSIGNED, POINTER, POINTER}, SIGNED},
    {__NR_getuid, "getuid", 0, {0}, UNSIGNED},
    {__NR_syslog, "syslog", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_getgid, "getgid", 1, {0}, UNSIGNED},
    {__NR_setuid, "setuid", 1, {UNSIGNED}, SIGNED},
    {__NR_setgid, "setgid", 1, {UNSIGNED}, SIGNED},
    {__NR_geteuid, "geteuid", 0, {0}, UNSIGNED},
    {__NR_getegid, "getegid", 0, {0}, UNSIGNED},
    {__NR_setpgid, "setpgid", 2, {UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_getppid, "getppid", 0, {0}, UNSIGNED},
    {__NR_getpgrp, "getpgrp", 0, {0}, UNSIGNED},
    {__NR_setsid, "setsid", 0, {0}, UNSIGNED},
    {__NR_setreuid, "setreuid", 2, {UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_setregid, "setregid", 2, {UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_getgroups, "getgroups", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_setgroups, "setgroups", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_setresuid, "setresuid", 3, {UNSIGNED, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_getresuid, "getresuid", 3, {POINTER, POINTER, POINTER}, SIGNED},
    {__NR_setresgid, "setresgid", 3, {UNSIGNED, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_getresgid, "getresgid", 3, {POINTER, POINTER, POINTER}, SIGNED},
    {__NR_getpgid, "getpgid", 1, {UNSIGNED}, UNSIGNED},
    {__NR_setfsuid, "setfsuid", 1, {UNSIGNED}, SIGNED},
    {__NR_setfsgid, "setfsgid", 1, {UNSIGNED}, SIGNED},
    {__NR_getsid, "getsid", 1, {UNSIGNED}, UNSIGNED},
    {__NR_capget, "capget", 2, {UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_capset, "capset", 2, {UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_rt_sigpending, "rt_sigpending", 1, {POINTER}, SIGNED},
    {__NR_rt_sigtimedwait, "rt_sigtimedwait", 3, {POINTER, POINTER, POINTER},
     SIGNED},
    {__NR_rt_sigqueueinfo, "rt_sigqueueinfo", 3, {SIGNED, SIGNED, UNSIGNED},
     SIGNED},
    {__NR_rt_sigsuspend, "rt_sigsuspend", 1, {POINTER}, SIGNED},
    {__NR_sigaltstack, "sigaltstack", 2, {POINTER, POINTER}, SIGNED},
    {__NR_utime, "utime", 2, {POINTER, POINTER}, SIGNED},
    {__NR_mknod, "mknod", 3, {POINTER, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_uselib, "ni_syscall", 1, {POINTER}, SIGNED},
    {__NR_personality, "personality", 1, {UNSIGNED}, SIGNED},
    {__NR_ustat, "ustat", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_statfs, "statfs", 2, {POINTER, POINTER}, SIGNED},
    {__NR_fstatfs, "fstatfs", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_sysfs, "sysfs", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_getpriority, "getpriority", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_setpriority, "setpriority", 3, {SIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_sched_setparam, "sched_setparam", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_sched_getparam, "sched_getparam", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_sched_setscheduler, "sched_setscheduler", 3, {UNSIGNED, SIGNED,
							POINTER}, SIGNED},
    {__NR_sched_getscheduler, "sched_getscheduler", 1, {UNSIGNED}, SIGNED},
    {__NR_sched_get_priority_max, "sched_get_priority_max", 1, {SIGNED},
     SIGNED},
    {__NR_sched_get_priority_min, "sched_get_priority_min", 1, {SIGNED},
     SIGNED},
    {__NR_sched_rr_get_interval, "sched_rr_get_interval", 2, {SIGNED, POINTER},
     SIGNED},
    {__NR_mlock, "mlock", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_munlock, "munlock", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_mlockall, "mlockall", 1, {SIGNED}, SIGNED},
    {__NR_munlockall, "munlockall", 0, {0}, SIGNED},
    {__NR_vhangup, "vhangup", 0, {0}, SIGNED},
    {__NR_modify_ldt, "modify_ldt", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_pivot_root, "pivot_root", 2, {POINTER, POINTER}, SIGNED},
    {__NR__sysctl, "sysctl", 1, {POINTER}, SIGNED},
    {__NR_prctl, "prctl", 5, {SIGNED, UNSIGNED, UNSIGNED, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_arch_prctl, "arch_prctl", 2, {SIGNED, UNSIGNED}, SIGNED},
    {__NR_adjtimex, "adjtimex", 1, {POINTER}, SIGNED},
    {__NR_setrlimit, "setrlimit", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_chroot, "chroot", 1, {POINTER}, SIGNED},
    {__NR_sync, "sync", 0, {0}, VOID},
    {__NR_acct, "acct", 1, {POINTER}, SIGNED},
    {__NR_settimeofday, "settimeofday", 2, {POINTER, POINTER}, SIGNED},
    {__NR_mount, "mount", 5, {POINTER, POINTER, POINTER, UNSIGNED, POINTER},
     SIGNED},
    {__NR_umount2, "umount2", 2, {POINTER, SIGNED}, SIGNED},
    {__NR_swapon, "swapon", 2, {POINTER, SIGNED}, SIGNED},
    {__NR_swapoff, "swapoff", 1, {POINTER}, SIGNED},
    {__NR_reboot, "reboot", 4, {SIGNED, SIGNED, SIGNED, POINTER}, SIGNED},
    {__NR_sethostname, "sethostname", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_setdomainname, "setdomainname", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_iopl, "iopl", 1, {SIGNED}, SIGNED},
    {__NR_ioperm, "ioperm", 3, {UNSIGNED, UNSIGNED, SIGNED}, SIGNED},
    {__NR_create_module, "create_module", 0, {0}, SIGNED},
    {__NR_init_module, "init_module", 2, {POINTER, POINTER}, SIGNED},
    {__NR_delete_module, "delete_module", 1, {POINTER}, SIGNED},
    {__NR_get_kernel_syms, "get_kernel_syms", 0, {0}, SIGNED},
    {__NR_query_module, "query_module", 0, {0}, SIGNED},
    {__NR_quotactl, "quotactl", 4, {SIGNED, POINTER, SIGNED, UNSIGNED},
     SIGNED},
    {__NR_nfsservctl, "nfsservctl", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_getpmsg, "getpmsg", 0, {0}, SIGNED},
    {__NR_putpmsg, "putpmsg", 0, {0}, SIGNED},
    {__NR_afs_syscall, "afs_syscall", 0, {0}, SIGNED},
    {__NR_tuxcall, "tuxcall", 0, {0}, SIGNED},
    {__NR_security, "security", 0, {0}, SIGNED},
    {__NR_gettid, "gettid", 0, {0}, UNSIGNED},
    {__NR_readahead, "readahead", 3, {SIGNED, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_setxattr, "setxattr", 5, {POINTER, POINTER, POINTER, UNSIGNED,
				    SIGNED}, SIGNED},
    {__NR_lsetxattr, "lsetxattr", 5, {POINTER, POINTER, POINTER, UNSIGNED,
				      SIGNED}, SIGNED},
    {__NR_fsetxattr, "fsetxattr", 5, {SIGNED, POINTER, POINTER, UNSIGNED,
				      SIGNED}, SIGNED},
    {__NR_getxattr, "getxattr", 4, {POINTER, POINTER, POINTER, UNSIGNED},
     SIGNED},
    {__NR_lgetxattr, "lgetxattr", 4, {POINTER, POINTER, POINTER, UNSIGNED},
     SIGNED},
    {__NR_fgetxattr, "fgetxattr", 4, {SIGNED, POINTER, POINTER, UNSIGNED},
     SIGNED},
    {__NR_listxattr, "listxattr", 3, {POINTER, POINTER, UNSIGNED}, SIGNED},
    {__NR_llistxattr, "llistxattr", 3, {POINTER, POINTER, SIGNED}, SIGNED},
    {__NR_flistxattr, "flistxattr", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_removexattr, "removexattr", 2, {POINTER, POINTER}, SIGNED},
    {__NR_lremovexattr, "lremovexattr", 2, {POINTER, POINTER}, SIGNED},
    {__NR_fremovexattr, "fremovexattr", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_tkill, "tkill", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_time, "time", 1, {POINTER}, UNSIGNED},
    {__NR_futex, "futex", 6, {POINTER, SIGNED, SIGNED, POINTER, POINTER,
			      SIGNED}, SIGNED},
    {__NR_sched_setaffinity, "sched_setaffinity", 3, {UNSIGNED, UNSIGNED,
						      POINTER}, SIGNED},
    {__NR_sched_getaffinity, "sched_getaffinity", 3, {UNSIGNED, UNSIGNED,
						      POINTER}, SIGNED},
    {__NR_set_thread_area, "ni_syscall", 0, {0}, SIGNED},
    {__NR_io_setup, "io_setup", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_io_destroy, "io_destroy", 1, {UNSIGNED}, SIGNED},
    {__NR_io_getevents, "io_getevents", 5, {UNSIGNED, SIGNED, SIGNED, POINTER,
					    POINTER}, SIGNED},
    {__NR_io_submit, "io_submit", 3, {UNSIGNED, SIGNED, POINTER}, SIGNED},
    {__NR_io_cancel, "io_cancel", 3, {UNSIGNED, POINTER, POINTER}, SIGNED},
    {__NR_get_thread_area, "ni_syscall", 0, {0}, SIGNED},
    {__NR_lookup_dcookie, "lookup_dcookie", 3, {UNSIGNED, POINTER, UNSIGNED},
     SIGNED},
    {__NR_epoll_create, "epoll_create", 1, {SIGNED}, SIGNED},
    {__NR_epoll_ctl_old, "ni_syscall", 0, {0}, SIGNED},
    {__NR_epoll_wait_old, "ni_syscall", 0, {0}, SIGNED},
    {__NR_remap_file_pages, "remap_file_pages", 5, {POINTER, UNSIGNED, SIGNED,
						    SIGNED, SIGNED}, SIGNED},
    {__NR_getdents64, "getdents64", 3, {UNSIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_set_tid_address, "set_tid_address", 1, {POINTER}, SIGNED},
    {__NR_restart_syscall, "restart_syscall", 0, {0}, SIGNED},
    {__NR_semtimedop, "semtimedop", 4, {SIGNED, POINTER, UNSIGNED, POINTER},
     SIGNED},
    {__NR_fadvise64, "fadvise64", 4, {SIGNED, UNSIGNED, UNSIGNED, SIGNED},
     SIGNED},
    {__NR_timer_create, "timer_create", 3, {UNSIGNED, POINTER, POINTER},
     SIGNED},
    {__NR_timer_settime, "timer_settime", 4, {UNSIGNED, SIGNED, POINTER,
					      POINTER}, SIGNED},
    {__NR_timer_gettime, "timer_gettime", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_timer_getoverrun, "timer_getoverrun", 1, {UNSIGNED}, SIGNED},
    {__NR_timer_delete, "timer_delete", 1, {UNSIGNED}, SIGNED},
    {__NR_clock_settime, "clock_settime", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_clock_gettime, "clock_gettime", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_clock_getres, "clock_getres", 2, {UNSIGNED, POINTER}, SIGNED},
    {__NR_clock_nanosleep, "clock_nanosleep", 4, {UNSIGNED, SIGNED, POINTER,
						  POINTER}, SIGNED},
    {__NR_exit_group, "exit_group", 1, {SIGNED}, VOID},
    {__NR_epoll_wait, "epoll_wait", 4, {SIGNED, POINTER, SIGNED, SIGNED},
     SIGNED},
    {__NR_epoll_ctl, "epoll_ctl", 4, {SIGNED, SIGNED, SIGNED, POINTER},
     SIGNED},
    {__NR_tgkill, "tgkill", 3, {SIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_utimes, "utimes", 2, {POINTER, POINTER}, SIGNED},
    {__NR_vserver, "ni_syscall", 0, {0}, SIGNED},
    {__NR_mbind, "mbind", 6, {POINTER, UNSIGNED, SIGNED, POINTER, UNSIGNED,
			      UNSIGNED}, SIGNED},
    {__NR_set_mempolicy, "set_mempolicy", 3, {SIGNED, POINTER, UNSIGNED},
     SIGNED},
    {__NR_get_mempolicy, "get_mempolicy", 5, {POINTER, POINTER, UNSIGNED,
					      UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_mq_open, "mq_open", 2, {POINTER, SIGNED}, UNSIGNED},
    {__NR_mq_unlink, "mq_unlink", 1, {POINTER}, UNSIGNED},
    {__NR_mq_timedsend, "mq_timedsend", 5, {UNSIGNED, POINTER, UNSIGNED,
					    UNSIGNED, POINTER}, UNSIGNED},
    {__NR_mq_timedreceive, "mq_timedreceive", 5, {UNSIGNED, POINTER, UNSIGNED,
						  POINTER, POINTER}, SIGNED},
    {__NR_mq_notify, "mq_notify", 2, {UNSIGNED, POINTER}, UNSIGNED},
    {__NR_mq_getsetattr, "mq_getsetattr", 3, {UNSIGNED, POINTER, POINTER},
     UNSIGNED},
    {__NR_kexec_load, "kexec_load", 4, {UNSIGNED, UNSIGNED, POINTER, UNSIGNED},
     SIGNED},
    {__NR_waitid, "waitid", 4, {UNSIGNED, UNSIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_add_key, "add_key", 5, {POINTER, POINTER, POINTER, UNSIGNED,
				  UNSIGNED}, UNSIGNED},
    {__NR_request_key, "request_key", 4, {POINTER, POINTER, POINTER, UNSIGNED},
     UNSIGNED},
    {__NR_keyctl, "keyctl", 2, {SIGNED, VARIABLE}, SIGNED},
    {__NR_ioprio_set, "ioprio_set", 3, {SIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_ioprio_get, "ioprio_get", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_inotify_init, "inotify_init", 0, {0}, SIGNED},
    {__NR_inotify_add_watch, "inotify_add_watch", 3, {SIGNED, POINTER,
						      UNSIGNED}, SIGNED},
    {__NR_inotify_rm_watch, "inotify_rm_watch", 2, {SIGNED, UNSIGNED}, SIGNED},
    {__NR_migrate_pages, "migrate_pages", 4, {SIGNED, UNSIGNED, POINTER,
					      POINTER}, SIGNED},
    {__NR_openat, "openat", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_mkdirat, "mkdirat", 3, {SIGNED, POINTER, UNSIGNED}, SIGNED},
    {__NR_mknodat, "mknodat", 4, {SIGNED, POINTER, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_fchownat, "fchownat", 5, {SIGNED, POINTER, UNSIGNED, UNSIGNED,
				    SIGNED}, SIGNED},
    {__NR_futimesat, "futimesat", 3, {SIGNED, POINTER, POINTER}, SIGNED},
    {__NR_newfstatat, "newfstatat", 4, {SIGNED, POINTER, POINTER, SIGNED},
     SIGNED},
    {__NR_unlinkat, "unlinkat", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_renameat, "renameat", 4, {SIGNED, POINTER, SIGNED, POINTER}, SIGNED},
    {__NR_linkat, "linkat", 5, {SIGNED, POINTER, SIGNED, POINTER, SIGNED},
     SIGNED},
    {__NR_symlinkat, "symlinkat", 3, {POINTER, SIGNED, POINTER}, SIGNED},
    {__NR_readlinkat, "readlinkat", 4, {SIGNED, POINTER, POINTER, UNSIGNED},
     SIGNED},
    {__NR_fchmodat, "fchmodat", 4, {SIGNED, POINTER, UNSIGNED, SIGNED},
     SIGNED},
    {__NR_faccessat, "faccessat", 4, {SIGNED, POINTER, SIGNED, SIGNED},
     SIGNED},
    {__NR_pselect6, "pselect6", 6, {SIGNED, POINTER, POINTER, POINTER, POINTER,
				    POINTER}, SIGNED},
    {__NR_ppoll, "ppoll", 4, {POINTER, UNSIGNED, POINTER, POINTER}, SIGNED},
    {__NR_unshare, "unshare", 1, {SIGNED}, SIGNED},
    {__NR_set_robust_list, "set_robust_list", 2, {POINTER, UNSIGNED}, SIGNED},
    {__NR_get_robust_list, "get_robust_list", 3, {SIGNED, POINTER, POINTER},
     SIGNED},
    {__NR_splice, "splice", 6, {SIGNED, POINTER, SIGNED, POINTER, UNSIGNED,
				UNSIGNED}, SIGNED},
    {__NR_tee, "tee", 4, {SIGNED, SIGNED, UNSIGNED, UNSIGNED}, SIGNED},
    {__NR_sync_file_range, "sync_file_range", 4, {SIGNED, UNSIGNED, UNSIGNED,
						  UNSIGNED}, SIGNED},
    {__NR_vmsplice, "vmsplice", 4, {SIGNED, POINTER, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_move_pages, "move_pages", 6, {SIGNED, UNSIGNED, POINTER, POINTER,
					POINTER, SIGNED}, SIGNED},
    {__NR_utimensat, "utimensat", 4, {SIGNED, POINTER, POINTER, SIGNED},
     SIGNED},
    {__NR_epoll_pwait, "epoll_pwait", 5, {SIGNED, POINTER, SIGNED, SIGNED,
					  POINTER}, SIGNED},
    {__NR_signalfd, "signalfd", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_timerfd_create, "timerfd_create", 2, {SIGNED, SIGNED}, SIGNED},
    {__NR_eventfd, "eventfd", 2, {UNSIGNED, SIGNED}, SIGNED},
    {__NR_fallocate, "fallocate", 4, {SIGNED, SIGNED, UNSIGNED, UNSIGNED},
     SIGNED},
    {__NR_timerfd_settime, "timerfd_settime", 4, {SIGNED, SIGNED, POINTER,
						  POINTER}, SIGNED},
    {__NR_timerfd_gettime, "timerfd_gettime", 2, {SIGNED, POINTER}, SIGNED},
    {__NR_accept4, "accept4", 4, {SIGNED, POINTER, POINTER, SIGNED}, SIGNED},
    {__NR_signalfd4, "signalfd4", 3, {SIGNED, POINTER, SIGNED}, SIGNED},
    {__NR_eventfd2, "eventfd2", 2, {UNSIGNED, SIGNED}, SIGNED},
    {__NR_epoll_create1, "epoll_create1", 1, {SIGNED}, SIGNED},
    {__NR_dup3, "dup3", 3, {SIGNED, SIGNED, SIGNED}, SIGNED},
    {__NR_pipe2, "pipe2", 2, {POINTER, SIGNED}, SIGNED},
    {__NR_inotify_init1, "inotify_init1", 1, {SIGNED}, SIGNED},
    {__NR_preadv, "preadv", 4, {SIGNED, POINTER, SIGNED, UNSIGNED}, SIGNED},
    {__NR_pwritev, "pwritev", 4, {SIGNED, POINTER, SIGNED, UNSIGNED}, SIGNED},
    {__NR_rt_tgsigqueueinfo, "rt_tgsigqueueinfo", 3, {SIGNED, SIGNED, SIGNED},
     SIGNED},
    {__NR_perf_event_open, "perf_event_open", 0, {0}, SIGNED},
    {__NR_recvmmsg, "recvmmsg", 5, {SIGNED, POINTER, UNSIGNED, UNSIGNED,
				    POINTER}, SIGNED}
  };
